function blurImage = blurInFourierSpace(inImage,kernelSize)
    
    [rows, cols] = size(inImage);
    
    gaus_kernel = gaussianKernel2D(kernelSize);
    gaus_kernel = pad_matrix(gaus_kernel, rows, cols);
    gaus_kernel = ifftshift(gaus_kernel);
    
    gaus_kernel = DFT2(gaus_kernel);
    dft = DFT2(inImage);
    
    mul_res = gaus_kernel .* dft;
    
    blurImage = IDFT2(mul_res);
    
    figure;
    imshow(blurImage);

end

function ret_val = pad_matrix(matrix, to_rows, to_cols)

    kernel_size = size(matrix, 1);

    a = floor(to_rows/2) + 1 - floor(kernel_size/2) - 1;
    row_pad_before = a;
    row_pad_after = a - mod(to_rows + 1, 2);
    
    b = floor(to_cols/2) + 1 - floor(kernel_size/2) - 1;
    col_pad_before = b;
    col_pad_after = b - mod(to_cols + 1, 2);
    
    padd_matrix = padarray(matrix, [row_pad_before, col_pad_before], 'pre');
    ret_val = padarray(padd_matrix, [row_pad_after, col_pad_after], 'post');
    
end
