function [imEq, histOrig, histEq] = histogramEqualize(imOrig)
    % perform histogram equalization for image
    
    simpleImShow(imOrig);

    [mat, rgbIm, imOrig] = imageInput(imOrig);
    
    [histOrig, ~] = imhist(mat);
    cs = cumsum(histOrig);
    
    lut = cs / numel(mat) * 255;
    
    lut = uint8(round(lut));
    
    % do linear streching before applying the lookup table
    minVal = min(lut(histOrig>0));
    maxVal = max(lut(histOrig>0));
    
    if (minVal > 0 || maxVal < 255)
        diff = maxVal - minVal;
        lut = lut * (255 / diff) - minVal/diff;
    end
    %
    
    imEq = intlut(mat, lut);
    [histEq, ~] = imhist(imEq);
    
    imEq = imageOutput(imOrig, imEq, rgbIm);
    
    simpleImShow(imEq);
    
end