function [ind1,ind2] = matchFeatures(desc1,desc2,minScore)
% MATCHFEATURES Match feature descriptors in desc1 and desc2.
% Arguments:
% desc1 − A kxkxn1 feature descriptor matrix.
% desc2 − A kxkxn2 feature descriptor matrix.
% minScore − Minimal match score between two descriptors required to be
% regarded as matching.
% Returns:
% ind1,ind2 − These are m−entry arrays of match indices in desc1 and desc2.
%
% Note:
% 1. The descriptors of the ith match are desc1(ind1(i)) and desc2(ind2(i)).
% 2. The number of feature descriptors n1 generally differs from n2
% 3. ind1 and ind2 have the same length.

    
    f0 = size(desc1, 3);
    f1 = size(desc2, 3);

    k = size(desc1, 1);
    
    n1 = size(desc1, 3);
    n2 = size(desc2, 3);

    t1 = ipermute(desc1, [2 1 3]);
    t1 = reshape(t1, [k*k n1])';
    
    t2 = ipermute(desc2, [2 1 3]);
    t2 = reshape(t2, [k*k n2]);
    
    s_mat = t1 * t2;
    
    f0max = ones(1, f0);
    f1max = ones(1, f1);
    
    for i=1:f0
       f0max(i) = second_max(s_mat(i,:));
    end
    
    for j=1:f1
       f1max(j) = second_max(s_mat(:,j));
    end
    
    ind1 = [];
    ind2 = [];
    
    for i=1:f0
        for j=1:f1
            val = s_mat(i,j);
            if val >= f0max(i) && val >= f1max(j) && val >= minScore
                ind1 = [ind1 i];
                ind2 = [ind2 j];
            end
        end
    end

end

function second_max = second_max(vec)

    second_max = max(vec(vec<max(vec)));

end