function fourierSignal = DFT(signal)

    dimen = numel(signal);
    
    vec = exp((-2 * pi * (0:dimen-1) * 1i)/dimen);
    vec = vander(vec);
    
    vandermonde = fliplr(vec);
    
    fourierSignal = (vandermonde * signal').';
    
end