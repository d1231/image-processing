danny12345

Question answers:

Q: Why did you get two different magnitude images?
A: 
Because the two operators we did are different. In the image space
we used approximation of the defenition of deravtive and in the fourier
space we had function and use the real defenition of deravtive to compute
the resulting magnitude image.

Q: What happens if the center of the gaussian (in the space domain) will not be at the (0,0)
of the image? Why does it happen?
A: 
The picture will be shifted. 
That because each pixel will be shifted because each will be the average of different
surronding pixel.

Q: What is the difference between the two results (Blurring in image space and blurring in
fourier space)?
A:
The only difference is in the edges of the pictures. That is true because of the convultion
theroem.


Files:
blurInFourierSpace.m:
Gaussian bluring in fourier space.

blurInImageSpace.m:
Gaussian bluring in image space.

DFT.m, IDFT.m, DFT2.m, IDFT2.m, pure_idft.m, pure_idft2.m:
1d and 2d Fourier transformation and it's inverse.
The pure function return a complex number and not real number.

fourierDerivative.m:
Image deravtive in fourier space.

convDerivative.m:
Image deravtive by convulation.

gaussianKernel.m:
Compute an apporximation of gaussian kernel.

