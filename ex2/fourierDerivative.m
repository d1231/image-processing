function [ magnitude ] =  fourierDerivative( inImage )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

    [rows, cols] = size(inImage);

    % x axis
    dft = DFT2(inImage);
    m = floor(rows/2) ;
    vec = (-m:m - mod(rows + 1, 2));
    vec = circshift(vec', [m + 1, 0])';
    dia = diag(vec);
    dft = (dft.'*dia).';
    
    inverse = pure_idft2(dft);
    dx = 2i*pi/rows * inverse;
    
    % y axis
    dft = DFT2(inImage);
    m = floor(cols/2) ;
    vec = (-m:m - mod(cols + 1, 2));
    vec = circshift(vec', [m + 1, 0])';
    dia = diag(vec);
    dft = dft*dia;
    
    inverse = pure_idft2(dft);
    dy = 2i*pi / cols * inverse;
    
    magnitude = sqrt(abs(dx).^2+abs(dy).^2);
    
    figure;
    imshow(magnitude);
end

