function blend(pim1, pim2, pmask, pyrLevels, pyrKernel, maskKernel)
    
    im1 = imReadAndConvert(pim1, 2);
    figure; imshow(im1);
    
    im2 = imReadAndConvert(pim2, 2);
    figure; imshow(im2);
    
    mask = imReadAndConvert(pmask, 1);
    figure; imshow(mask);

    im = zeros(size(im1));

    for i=1:3
        im(:,:,i) = pyramidBlending(im1(:,:,i), im2(:,:,i), mask, pyrLevels, pyrKernel, maskKernel);
    end

    figure;
    imshow(im);
    
end