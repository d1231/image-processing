function imYIQ = transformRGB2YIQ(imRGB) 

    transformMatrix = [0.299 , 0.587  , 0.114;
                       0.596 , -0.275 , -0.321;
                       0.212 , -0.523 , 0.311];
    
    [width, height, ~] = size(imRGB);
    
    mat = transformMatrix*[
        reshape(imRGB(:,:,1).',1,[]);
        reshape(imRGB(:,:,2).',1,[]);
        reshape(imRGB(:,:,3).',1,[])];
    
    imYIQ(:,:,1) = reshape(mat(1,:), [height width]).';
    imYIQ(:,:,2) = reshape(mat(2,:), [height width]).';
    imYIQ(:,:,3) = reshape(mat(3,:), [height width]).';
    
end