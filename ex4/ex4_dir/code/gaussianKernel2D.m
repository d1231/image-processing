function kernel = gaussianKernel2D(order)
    
    vec = 1;

    for i=1:order-1
       vec = conv2([1 1], vec); 
    end
    
    kernel = conv2(vec, vec');
    
    % normalize
    kernel = 1 / sum(sum(kernel)) * kernel;
    
end