danny12345

Question answers:

Q: What does it mean to multiply each level in a different value? What do we try to control on?
A: 
We try to control the intensities of the different frequencies in the image. 
The higher level pyramids are more representative of the higher frequency 
and lower level are more representative of the lower frequencies so the 
value decide how much we will give to each level.

Q: Blending is performed with different image filters
A:
We take in consideration more details from each image 
as we average a bigger surrounding each level.

Q: Blending is performed with a varying number of pyramid levels 
A:
We take in consideration more frequencies from each image
as we use more of low-pass filters.


Files:
blend.m:
Helper function to blend image and show what needs.

blending1|2.1/.2/.mask.jpg
blending images and masks

blendingExample1|2.m:
blending examples

displayPyramid.m:
show pyramid

expand.m/reduce.m:
expand and reduce operation for image helper function

renderPyramid.m:
render pyr

strech.m:
strech helper


