function [pyr, filter] = GaussianPyramid(im, maxLevels, filterSize)

    pyr = cell(maxLevels, 1);
    
    filter = gaussianKernel(filterSize);
    pyr{1} = im;
    
    for i=2:maxLevels
        pyr{i} = reduce(pyr{i-1}, filter);     
        
        % cut if reached the end
        mat_size = size(pyr{i});
        if (mat_size(1) == 16 || mat_size(2) == 16)
           pyr = pyr(1:i); 
           break
        end
    end

end