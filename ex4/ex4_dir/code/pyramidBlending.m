function imBlend = pyramidBlending(im1, im2, mask, maxLevels, filterSizeIm, filterSizeMask)

    [pyr1, filter] = LaplacianPyramid(im1, maxLevels, filterSizeIm);
    [pyr2, ~] = LaplacianPyramid(im2, maxLevels, filterSizeIm);
    [gpyr3, ~] = GaussianPyramid(double(mask), maxLevels, filterSizeMask);
   
    
    lout = cell(1, maxLevels);
    
    for i=1:maxLevels
        a = gpyr3{i};
        b = pyr1{i};
        c = pyr2{i};
        lout{i} = a.*b+(1-a).*c;
        
    end
    
    imBlend = LaplacianToImage(lout, filter, ones(1,maxLevels));

end