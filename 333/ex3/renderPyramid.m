function res = renderPyramid(pyr,levels)

    pyr_size = size(pyr{1});
    
    res = [strech(pyr{1})];

    for i=2:levels
       mat = pyr{i};
       mat = strech(mat);
       mat = pad(mat, pyr_size(1));
       res = [res mat];
    end

end

function mat = pad(mat, orig)

    [pad_to, ~] = size(mat);
    pad_to = orig - pad_to;
    mat = padarray(mat, [pad_to, 0], 'post');

end