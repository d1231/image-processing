function [imQuant, error] = quantizeImage(imOrig, nQuant, nIter)

    figure;
    imshow(imOrig);
    
    
    [mat, rgbIm, imOrig] = imageInput(imOrig);
    
    [histOrig, ~] = imhist(mat);
    cs = cumsum(histOrig);
    
    fun = @(A,B) min(A-B, B-A);
    
    % assumption nIter >= 1
    ttt=round(numel(mat)/nQuant*(0:nQuant));
    [~, z] = min(abs(bsxfun(fun,cs,ttt)));
    z(nQuant+1)=256;
    z = z-1;
    
    mulz = (0:255) .* histOrig';
    
    q =  calculateQ(histOrig, mulz, z, nQuant);
    error = zeros(1, nIter);
    error(1) = calcError(q, histOrig, z, nQuant);
    
    for i=2:nIter
        
        newZ = calcZ(q, nQuant);
        
        % test for convergence
        if (isequal(newZ, z))
            error = error(1:i-1);    
            break;
        end
        
        z = newZ;
        q =  calculateQ(histOrig, mulz, z, nQuant);
        error(i) = calcError(q, histOrig, z, nQuant);
              
    end
    
    lut = zeros(1,256);
    z = z + 1;
    for i=1:nQuant
        
        % take [z(i),z(i+1))
        lut(z(i):z(i+1)-1) = q(i);
    end
    
    % set last index the same as the last bin value
    lut(256)=q(nQuant);
    
    imQuant = intlut(im2uint8(mat), uint8(lut));
    
    imQuant = imageOutput(imOrig, imQuant, rgbIm);
    
    figure;
    imshow(imQuant);
    error = error';
    figure;
    plot(error);
        
end

function [q] = calculateQ(histOrig, mulz, z, nQuan)

    q=zeros(1, nQuan);

    for i=1:nQuan
       fac = (i ~= nQuan);
       vec = (z(i):z(i+1)-fac)+1; % +1 to fix index access
       q(i) = sum(mulz(vec)) / sum(histOrig(vec));
    end
    
    q = round(q);

end

function [z] = calcZ(q, nQuan)

    z = zeros(1, nQuan+1);
    
    % z(0)=0, z(nQuan)=255
    z(nQuan + 1) = 255;
    
    for i=2:nQuan
       z(i) = (q(i-1) + q(i)) /2 ; 
    end
    
    
    z = round(z);

end

function [err] = calcError(q, histOrig, z, nQuan)

    t = zeros(1, nQuan);
    for i=1:nQuan
        
        % take [z(i),z(i+1)), but remember to include the last one
        fac = (i ~= nQuan);
        
        vec = z(i):(z(i+1)-fac);
        vecIndex = vec+ 1;
        
        % calc error for bin number i
        t(i) = sum(((q(i)-vec).^2).*histOrig(vecIndex)');
        
    end
   
    err=sum(t);
end