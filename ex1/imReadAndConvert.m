function im = imReadAndConvert(filename, representation)
    % read image convert to representation (1 for greyscale, 2 for rgb)
    repr = imageRepresntation(filename);
    
    im = imread(filename);
    
    if (repr ~= 1 && representation == 1)
        im = rgb2gray(im);
    end

    im = im2double(im);
    
end

function representation = imageRepresntation(filename)
    % returns the image representation, 1 for greyscale image
    % 2 for rgb color
    info = imfinfo(filename);
    
    rep = info.ColorType;
    
    representation = -1;
    
    if (strcmp(rep, 'truecolor'))
        representation =  2;
    elseif (strcmp(rep, 'grayscale'))
        representation = 1;
    end
   
end