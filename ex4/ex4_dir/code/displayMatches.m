function displayMatches(im1,im2,pos1,pos2,inliners)
% DISPLAYMATCHES Display matched pt. pairs overlayed on given image pair.
% Arguments:
% im1,im2 − two grayscale images
% pos1,pos2 − Nx2 matrices containing n rows of [x,y] coordinates of matched
% points in im1 and im2 (i.e. the i’th match’s coordinate is
% pos1(i,:) in im1 and and pos2(i,:) in im2).
% inliers − A kx1 vector of


    size_dif = size(im1, 1) - size(im2, 1);
    
    if size_dif < 0
       im1 = padarray(im1, [-size_dif , 0], 'post');
    else if size_dif > 0
            im2 = padarray(im2, [size_dif , 0], 'post');
        end
    end

    displayed = [im1, im2];
    shift = size(im1, 2);
    pos2(:,1) = pos2(:,1) + shift;
    
    figure;
    imshow(displayed);
    
    hold on;
    
    plot(pos1(:,1), pos1(:,2), 'r.');
    plot(pos2(:,1), pos2(:,2), 'r.');
    plot([pos1(:,1) pos2(:, 1)]', [pos1(:,2), pos2(:,2)]', 'b-');
    plot([pos1(inliners,1) pos2(inliners, 1)]', [pos1(inliners,2), pos2(inliners,2)]', 'y-');
    
    hold off;
    
end