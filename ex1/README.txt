
Answer to question in 3.5:
If there is segment with zero pixles in it we will divide zero by zero and so
will get NaN for the relevent q(i) of the segment.

Image that I used:

- testImg1: With that image I tested the quantization. 
The image is with high level of grey and with that I could test how my program
deals with this kind of input.
Also I found the image in the internet and I had something to compare the result with.

- testImg2:
I found the image in the internet and I had something to compare the result with.

Files and function that I wrote:
histogramEqualize.m:
Contain the functions that does the histogram.

imDisplay.m:
Display image based on representation param input.

imReadAndConvert.m:
imReadAndConvert function that recevies image and convert image to requested represntation

imageRepresntation function that receives image filename and returns 1 or 2 
if the image is rgb or grey scale.

quantizeImage.m:
quantizeImage function that perform quantiztion for image.

calculateQ function that perform calculation of q.

calculateZ function that perform calculation of z.

calculateError function that perform calculation of the mean square error.

simpleImShow.m:
Shows image in new figure.

transformRGB2YIQ.m and transformYIQ2RGB.m:
Transform image from rgb 2 yiq or in the other direction.

imageInput.m:
returns the Y channel if rgb else return the original matrix return the matrix as uint8

imageOutput.m:
insert the mat into y channel and convert back to rgb if the original image was rgb 
convert matrix to doubles 
