
mat = ones(512,512);

for i=1:512
    mat(i,:) = [randperm(256) randperm(256)] - 1;
end

mat = mat / 255;
imhist(mat);

for z=1:50
    x = randperm(512, 2);
    t1 = mat(x(1));
    t2 = mat(x(2));
    mat(x(2)) = t1;
    mat(x(1)) = t2;
end

for z=1:50
    x = randperm(512, 2);
    t1 = mat(x(:,1));
    t2 = mat(x(:,2));
    mat(x(:,2)) = t1;
    mat(x(:,1)) = t2;
end

imhist(mat);
imshow(mat);