function imRGB = transformYIQ2RGB(imYIQ) 

    transformMatrix = [1 , 0.9557  , 0.6199;
                       1 , -0.2716 , -0.6469;
                       1 , -1.1082 , 1.7051];
    
    [width, height, ~] = size(imYIQ);
    
    % flat 3d matrix to 2d matrix and multiply by inverse
    mat = transformMatrix * [reshape(imYIQ(:,:,1).',1,[]);
           reshape(imYIQ(:,:,2).',1,[]);
           reshape(imYIQ(:,:,3).',1,[])] ;
    
    imRGB(:,:,1) = reshape(mat(1,:), [height width]).';
    imRGB(:,:,2) = reshape(mat(2,:), [height width]).';
    imRGB(:,:,3) = reshape(mat(3,:), [height width]).';
    
end