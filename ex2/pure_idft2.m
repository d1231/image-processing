function [ image ] = pure_idft2( fourierImage )

    [x, y] = size(fourierImage);
    image(1:x, 1:y) = 0;
    
    for i=(1:x)
       
        signal = fourierImage(i,:);
        fourierSignal = pure_idft(signal).';
        image(i,:) =  fourierSignal;
        
    end
    
    for i=(1:y)
       
        signal = image(:,i).';
        fourierSignal = pure_idft(signal).';
        image(:,i) =  fourierSignal;
        
    end

end

