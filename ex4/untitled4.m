im1 = imread('/home/danny/projects/university/imp/ex4/data/inp/examples/oxford1.jpg');
im2 = imread('/home/danny/projects/university/imp/ex4/data/inp/examples/oxford2.jpg');

im1 = im2double(rgb2gray(im1));
im2 = im2double(rgb2gray(im2));

pyr1 = GaussianPyramid(im1, 3, 3);
pyr2 = GaussianPyramid(im2, 3, 3);

[pos1,desc1] = findFeatures(pyr1);
[pos2,desc2] = findFeatures(pyr2);

[ind1,ind2] = matchFeatures(desc1, desc2, 0.7);

m1 = pos1(ind1,:);
m2 = pos2(ind2,:);

[H12,inliers] = ransacHomography(pos1(ind1,:),pos2(ind2,:),1500,9)

displayMatches(im1,im2,m1,m2,inliners)

imR{1} = im1;
imR{2} = im2;
Hpair{1} = H12;
Htot = accumulateHomographies(Hpair,1);
renderPanorama(imR,Htot); 