function imDisplay(filename, representation)
    % displays image based on representation

    im = imReadAndConvert(filename, representation);
    figure;
    imshow(im);
    impixelinfo;

end