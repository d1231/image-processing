function signal = IDFT(fourierSignal)

    signal = pure_idft(fourierSignal);
    signal = real(signal)';
    
end