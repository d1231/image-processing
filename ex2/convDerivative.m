function [ magnitude ] = convDerivative( inImage )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    dy = conv2(inImage, [1; 0; -1], 'same');
    dx = conv2(inImage, [1, 0, -1], 'same');
    
    magnitude = sqrt(abs(dx).^2+abs(dy).^2);
    
    figure;
    imshow(magnitude);
    
end

