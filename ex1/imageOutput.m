function [mat] = imageOutput(orig, mat, rgbIm) 
    % insert the mat into y channel and convert back to rgb if the original
    % image was rgb
    % convert matrix to doubles
    
    if (rgbIm)
       orig(:,:,1) = im2double(mat);
       mat = ntsc2rgb(orig);
    end
    
    mat = im2double(mat);
    
end
