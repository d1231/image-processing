function kernel = gaussianKernel(order)
    
    vec = 1;

    for i=1:order-1
       vec = conv2([1 1], vec); 
    end
    
    % normalize
    kernel =1 / sum(vec) *  vec;
    
end