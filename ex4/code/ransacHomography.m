function [H12,inliers] = ransacHomography(pos1,pos2,numIters,inlierTol)
% RANSACHOMOGRAPHY Fit homography to maximal inliers given point matches
% using the RANSAC algorithm.
% Arguments:
% pos1,pos2 − Two Nx2 matrices containing n rows of [x,y] coordinates of
% matched points.
% numIters − Number of RANSAC iterations to perform.
% inlierTol − inlier tolerance threshold.
% Returns:
% H12 − A 3x3 normalized homography matrix.
% inliers − A kx1 vector where k is the number of inliers, containing the indices in pos1/pos2 of the maximal set of
% inlier matches found.

    num_of_points = size(pos1, 1);
    best = 0;
    best_set = zeros(num_of_points);
    
    for i=1:numIters
       
        J = randsample(1:(num_of_points), 4);
        h12 = leastSquaresHomography(pos1(J,:), pos2(J,:));
        
        if (numel(h12) <= 0)
            continue
        end
        
        npos1 = applyHomography(pos1, h12);
        e = sqrt(sum(abs(pos2 - npos1).^2, 2)).^2;
        
        inliner_set = e<inlierTol;
        inliner_set_size = nnz(inliner_set);
        
        if best < inliner_set_size
           best =  inliner_set_size;
           best_set = inliner_set;
        end
        
    end
    
    
    H12 = leastSquaresHomography(pos1(best_set,:), pos2(best_set,:));
    inliers = find(best_set > 0);

end