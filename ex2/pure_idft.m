function [ signal ] = pure_idft( fourierSignal )
    
    dimen = numel(fourierSignal);
    
    vec = exp((-2 * pi * (0:dimen-1) * 1i)/dimen);
   
    vandermonde = (fliplr(vander(vec)).^-1 ) / dimen;
    
    signal = vandermonde * fourierSignal.';

end

