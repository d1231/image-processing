function [im] = expand(mat, filter)
    
    filter = 2*filter;
    mat_size = size(mat);
    im = zeros(mat_size(1)*2, mat_size(2)*2);
    im(1:2:end, 1:2:end) = mat;
    im = conv2(im, filter, 'same');
    im = conv2(im, filter', 'same');

end