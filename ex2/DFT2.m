function [ fourierImage ] = DFT2( image )

    [x, y] = size(image);
    fourierImage(1:x, 1:y) = 0;
    for i=(1:x)
       
        signal = image(i,:);
        fourierSignal = DFT(signal);
        fourierImage(i,:) =  fourierSignal;
        
    end
    for i=(1:y)
       
        signal = fourierImage(:,i)';
        fourierSignal = DFT(signal);
        fourierImage(:,i) =  fourierSignal;
        
    end

end

