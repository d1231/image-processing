function blurImage = blurInImageSpace(inImage,kernelSize)
    
    gaus_kernel = gaussianKernel2D(kernelSize);
    
    blurImage = conv2(inImage, gaus_kernel, 'same');

end