function pos = HarrisCornerDetector( im )
% HARRISCORNERDETECTOR Extract key points from the image.
% Arguments:
% im − nxm grayscale image to find key points inside.
% pos − A Nx2 matrix of [x,y] key points positions in im.


    lx = conv2(im, [1 0 1], 'same');
    ly = conv2(im, [1; 0; -1], 'same');

    lx = lx .* lx;
    ly = ly .* ly;
    lxly = lx .* ly;
    
    lx = blurInImageSpace(lx, 5);
    ly = blurInImageSpace(ly, 5);
    lxly = blurInImageSpace(lxly, 5);
    
    response_mat = (lx .* ly - lxly .* lxly) - 0.04 * ((lx + ly).^2);
    result = nonMaximumSuppression(response_mat);
    [x, y] = find(result);
    
    pos = horzcat(y, x);
    
end