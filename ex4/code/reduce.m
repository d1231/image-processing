function [im] = reduce(mat, kernel)

    im = conv2(mat, kernel, 'same');
    im = conv2(im, kernel', 'same');
    im = im(1:2:end, 1:2:end);

end