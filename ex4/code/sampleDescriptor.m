function desc = sampleDescriptor(im,pos,descRad)
% SAMPLEDESCRIPTOR Sample a MOPS−like descriptor at given positions in the image.
% Arguments:
% im − nxm grayscale image to sample within.
% pos − A Nx2 matrix of [x,y] descriptor positions in im.
% descRad − ”Radius” of descriptors to compute (see below).
% Returns:
% desc − A kxkxN 3−d matrix containing the ith descriptor
% at desc(:,:,i). The per−descriptor dimensions kxk are related to the
% descRad argument as follows k = 1+2∗descRad.


    sample_region = -descRad:descRad;
    
    s = size(pos,1);
    
    desc = zeros(1+2*descRad, 1+2*descRad, s);
    
    for i=1:s
       
        it_pos = pos(i,:);
        
        [regionx, regiony] = meshgrid(it_pos(1) + sample_region, it_pos(2) + sample_region);
        
        sample = interp2(im, regionx, regiony);
        sample(isnan(sample)) = 0;
        
        mean_val = mean2(sample);
        
        sample = sample - mean_val;
        
        sample = sample / norm(sample(:));
        
        desc(:,:,i) = sample;
        
    end

end
