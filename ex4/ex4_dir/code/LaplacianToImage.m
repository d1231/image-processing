function img = LaplacianToImage(lpyr, filter, coeffMultVec)

    lpyr{length(lpyr)} = lpyr{length(lpyr)} * coeffMultVec(length(lpyr));

    for i=length(lpyr)-1:-1:1
       lpyr{i} = lpyr{i}*coeffMultVec(i) + expand(lpyr{i+1}, filter); 
    end
    
    img = lpyr{1};
    
end