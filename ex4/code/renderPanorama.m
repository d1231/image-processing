function panorama = renderPanorama(im,H)
% RENDERPANORAMA Renders a set of images into a combined panorama image.
% Arguments:
% im − Cell array of n grayscale images.
% H − Cell array of n 3x3 homography matrices transforming the ith image
% coordinates to the panorama image coordinates.
% Returns:
% panorama − A grayscale panorama image composed of n vertical strips that
% were backwarped each from the relevant frame im{i} using homography H{i}.
    
    corners = cell(length(im), 1);
    center = cell(length(im), 1);
    
    con_strip = 60;
    
    % calculate cordinates for each image
    for i=1:length(im)
       image = im{i};
       lcorners = [
           1, 1; 
           1, size(image, 1); 
           size(image, 2), 1; 
           size(image, 2), size(image, 1)
       ];
       corners{i} = applyHomography(lcorners, H{i});
       
       center{i} = [size(image, 2) / 2, size(image, 1) / 2];
       center{i} = applyHomography(center{i}, H{i});
    end
    
    xmin = ceil(min(corners{1}(:,1)));
    xmax = ceil(max(corners{end}(:,1)));
    
    ymin = ceil(min(cellfun(@(x) min(x(:,2)), corners)));
    ymax = ceil(max(cellfun(@(x) max(x(:,2)), corners)));
    
    center = cellfun(@(x) x(1)+abs(xmin), center);
    
    inv_h = cell(length(H), 1);
    for i=1:length(H)
       inv_h{i} = inv(H{i});
    end
    
    strips=ones(length(im) + 1,1);
    
    for i=2:length(im)
       strips(i) = round((center(i-1) + center(i) ) / 2); 
    end
    
    strips(length(im) + 1) = xmax - xmin + 1;
    
    ipano = zeros(ymax - ymin + 1, xmax - xmin + 1);
    [xpano, ypano] = meshgrid(xmin:xmax, ymin:ymax);
    
    for i=1:length(im)
       
        start_strip = (i~=1)*con_strip;
        end_strip = (i~=length(im))*con_strip;
        strip_range = (strips(i)+start_strip):(strips(i+1)-end_strip);
        
        x2 = xpano(:, strip_range);
        y2 = ypano(:, strip_range);
        
        res = applyHomography([x2(:) y2(:)], inv_h{i});
        x1 = reshape(res(:,1), size(x2));
        y1 = reshape(res(:,2), size(y2));
        ipano(:, strip_range) = interp2(im{i}, x1, y1);
        
        if (i ~= 1)
            stich_range = (strips(i)-con_strip):(strips(i)+con_strip);
            ipano(:, stich_range) = stich(im{i-1}, im{i}, xpano, ypano, inv_h{i-1}, inv_h{i}, stich_range);
        end

    end
    
    ipano(isnan(ipano)) = 0;
    
    panorama = ipano;
    

end

function stiched_im = stich(im1, im2, xpano, ypano, h1, h2, stich_range)

    x2 = xpano(:, stich_range);
    y2 = ypano(:, stich_range);
        
    vec = [ones(30, 1)' linspace(1, 0, numel(stich_range)) zeros(30, 1)'];
    mask = repmat(vec, size(im1, 1), 1);
    
    res = applyHomography([x2(:) y2(:)], h1);
    x1 = reshape(res(:,1), size(x2));
    y1 = reshape(res(:,2), size(y2));
    im1 = interp2(im1, x1, y1);
    
    res = applyHomography([x2(:) y2(:)], h2);
    x1 = reshape(res(:,1), size(x2));
    y1 = reshape(res(:,2), size(y2));
    im2 = interp2(im2, x1, y1);
    
    mask = interp2(mask, x1, y1);
    imshow(mask);
    mask(isnan(im1)) = 0;
    mask(isnan(im2)) = 1;
    mask(isnan(mask)) = 0;
    im1(isnan(im1)) = 0;
    im2(isnan(im2)) = 0;
    
    
    % make the images next power of 2
    orig_size = size(im1);
    pow2 = nextpow2(orig_size);
    diff = 2.^pow2-orig_size;
    
    im1 = padarray(im1, diff, 'post');
    im2 = padarray(im2, diff, 'post');
    mask = padarray(mask, diff, 'post');
    
    stiched_im = pyramidBlending(im1, im2, mask, 4, 5, 5);
    stiched_im = stiched_im(1:orig_size(1), 1:orig_size(2));
    
end