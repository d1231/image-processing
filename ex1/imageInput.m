function [ mat, rgb, imOrig ] = imageInput( imOrig )
    % returns the Y channel if rgb
    % else return the original matrix
    % return the matrix as doubles


    % if im is rgb then matrix is 3d
    rgb = numel(size(imOrig)) >= 3;
    if (rgb)
        % convert to YIQ and get Y channel
        imOrig = rgb2ntsc(imOrig);
        mat = imOrig(:,:,1);
    else
        mat = imOrig;
    end

    mat = im2uint8(mat);
end

