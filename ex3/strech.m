
function mat = strech(mat)
    
    inlo = min(min(mat));
    inup = max(max(mat));
    outup = 1;
    
    mat = (mat - inlo) * ((outup)/(inup-inlo));
    
end