function [pyr, filter] = LaplacianPyramid(im, maxLevels, filterSize)

    [gaus_pyr, filter] = GaussianPyramid(im, maxLevels, filterSize);
    
    pyr = cell(length(gaus_pyr), 1);
    
    pyr{end} = gaus_pyr{end};
    
    vec = 1:length(pyr)-1;
    for i=vec(end:-1:1)
        expanded = expand(gaus_pyr{i+1}, filter);
        pyr{i} = gaus_pyr{i}-expanded;
    end

end
