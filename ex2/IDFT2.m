function [ image ] = IDFT2( fourierImage )
    
    image = pure_idft2(fourierImage);

    image = real(image);
end

